package pi;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.*;
//import java.util.ArrayList;
//import java.util.List;

public class PiCalculator {
    private static final int partitions = 10000000;
    private static final int SCALE = 75;

//    public static class Test implements Runnable {
//        private List<BigDecimal> lbd;
//        private int i;
//        @Override
//        public void run() {
//            lbd.set(0, lbd.get(0).add(PiCalculator.partitionValueCalculator(partitions, i)));
//        }
//
//        public void setI(int i) {
//            this.i = i;
//        }
//
//        public void setLbd(List<BigDecimal> lbd) {
//            this.lbd = lbd;
//        }
//    }

    public static BigDecimal partitionValueCalculator(int partitions, int iterator) {
        BigDecimal bIterator = new BigDecimal(iterator);
        BigDecimal bPartitions = new BigDecimal(partitions);
        BigDecimal constant4 = new BigDecimal(4);
        BigDecimal constant1 = new BigDecimal(1);
        BigDecimal constant0_5 = new BigDecimal("0.5");

        BigDecimal aux = bIterator.subtract(constant0_5).divide(bPartitions, SCALE, RoundingMode.HALF_UP);
        aux = aux.multiply(aux);
        aux = constant1.add(aux);
        return constant4.divide(aux, SCALE, RoundingMode.HALF_UP).divide(bPartitions, SCALE, RoundingMode.HALF_UP);
    }

    public static void main(String[] args) {
        BigDecimal test = new BigDecimal("0.0");
        BigDecimal PI = new BigDecimal("3.141592653589793238462643383279502884197169399375105820974944592307816406286");
//        List<BigDecimal> list = new ArrayList<>();
//        list.add(test);

//        List<Thread> lt = new ArrayList<>();

//        for (int i = 1; i <= partitions; i++) {
//            Test aux = new Test();
//            aux.setI(i);
//            aux.setLbd(list);
//            Thread t = new Thread(aux);
//            lt.add(t);
//            t.start();
//        }
//        for (Thread t: lt) {
//            try {
//                t.join();
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }

//        long st = System.nanoTime();
//        ExecutorService es = Executors.newFixedThreadPool(4);
//        List<Future<BigDecimal>> lfb = new ArrayList<>();
//        for (int i = 1; i <= partitions; i++) {
//            final int aux = i;
//            lfb.add(es.submit(() -> PiCalculator.partitionValueCalculator(partitions, aux)));
//        }
//        es.shutdown();
//        for (Future<BigDecimal> e: lfb) {
//            try {
//                test = test.add(e.get());
//            } catch (InterruptedException | ExecutionException interruptedException) {
//                interruptedException.printStackTrace();
//            }
//        }
//        long et = System.nanoTime();

//        List<CompletableFuture<BigDecimal>> lcfbi = new ArrayList<>();
//
//        long st = System.nanoTime();
//        for (int i = 1; i <= partitions; i++) {
//            int finalI = i;
//            lcfbi.add(CompletableFuture.supplyAsync(() -> PiCalculator.partitionValueCalculator(partitions, finalI)));
//        }
//        long et = System.nanoTime();
//
//        long st2 = System.nanoTime();
//        for (CompletableFuture<BigDecimal> e: lcfbi) {
//            try {
//                test = test.add(e.get());
//            } catch (InterruptedException | ExecutionException interruptedException) {
//                interruptedException.printStackTrace();
//            }
//        }
//        long et2 = System.nanoTime();

        CompletableFuture<BigDecimal> cf = CompletableFuture.supplyAsync(() -> PiCalculator.partitionValueCalculator(partitions, 1));

        long st = System.nanoTime();
        for (int i = 2; i <= partitions; i++) {
            int finalI = i;
            CompletableFuture<BigDecimal> aux = CompletableFuture.supplyAsync(() -> PiCalculator.partitionValueCalculator(partitions, finalI));
            cf = cf.thenCombine(aux, BigDecimal::add);
        }
        test = cf.join();
        long et = System.nanoTime();

        BigDecimal dif = test.abs().subtract(PI).divide(BigDecimal.valueOf(100), SCALE, RoundingMode.HALF_UP);

        System.out.println("Real value: "+ PI);
        System.out.println("Calculated value: "+ test);
        System.out.println("Error: "+ dif+"%");
        System.out.println("Elapsed time calculating the value of each partition: "+(et-st)/Math.pow(10, 9));
    }
}

//import java.math.BigDecimal;
//import java.math.RoundingMode;
//
//public class PiCalculator {
//    BigDecimal result = new BigDecimal("0.0");
////    double result = 0;
//    private static final int partitions = 10000000;
//    private static final int SCALE = 75;
//
//    class Pi implements Runnable {
//        int start;
//        int end;
//
//        public Pi(int start, int end) {
//            this.start = start;
//            this.end = end;
//        }
//
//        @Override
//        public void run() {
//            for (int i = start; i < end; i++) {
//                BigDecimal bIterator = new BigDecimal(i);
//                BigDecimal bPartitions = new BigDecimal(partitions);
//                BigDecimal constant4 = new BigDecimal(4);
//                BigDecimal constant1 = new BigDecimal(1);
//                BigDecimal constant0_5 = new BigDecimal("0.5");
//
//                BigDecimal aux = bIterator.subtract(constant0_5).divide(bPartitions, SCALE, RoundingMode.HALF_UP);
//                aux = aux.multiply(aux);
//                aux = constant1.add(aux);
//                result = result.add(constant4.divide(aux, SCALE, RoundingMode.HALF_UP).divide(bPartitions, SCALE, RoundingMode.HALF_UP));
//            }
////            System.out.println(Thread.currentThread().getName() + " result = " + result);
//        }
//    }
//
//    public static void main(String[] args) throws InterruptedException {
//        int maxThreads = 2;
//        int maxValuePerThread = partitions / maxThreads;
//        int start = 0;
//        int end = maxValuePerThread;
//        PiCalculator pc = new PiCalculator();
//        Thread[] threads = new Thread[2];
//        for (int i = 0; i < 2; i++) {
//            threads[i] = new Thread(pc.new Pi(start, end));
//            threads[i].start();
//            start = end;
//            end += maxValuePerThread;
//        }
//
//        for(int i = 0; i < 2; i++) {
//            threads[i].join();
//        }
//        System.out.println("Final result = " + pc.result);
//    }
//}