package sort;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class QuickSort {
    private List<Integer> array = new ArrayList<Integer>();
    private int len = 750;

    public static void main(String[] args) {
        long start, end;
        float time;

        QuickSort qs = new QuickSort();
        qs.fillIn(qs.array);
        List<Integer> aux = new ArrayList<>();
        aux.addAll(qs.array);

        System.out.println("Unsorted array:\n" + qs.array + "\n");
        start = System.nanoTime();
        qs.quickParallel(qs.array, 0, qs.len - 1);
        end = System.nanoTime();
        time = (float)((end - start)/Math.pow(10, 9));
        System.out.println("Sorted array:\n" + qs.array);
        System.out.println("The time used to sort in parallel " + qs.len + " elements was: " + time+ "\n");

        start = System.nanoTime();
        qs.quick(aux, 0, qs.len - 1);
        end = System.nanoTime();
        time = (float)((end - start)/Math.pow(10, 9));
        System.out.println("Sorted array:\n" + aux);
        System.out.println("The time used to sort sequentially " + qs.len + " elements was: "+ time);
    }

    private void fillIn(List<Integer> array) {
        for(int i = 0; i < this.len; i++) {
            if(Math.random() <= 0.5) {
                array.add((int)(Math.random()*(len/10)) + 1);
            }else {
                array.add(-1*(int)(Math.random()*(len/10)) + 1);
            }
        }
    }

    private int partition(List<Integer> array, int start, int end) {
        int pivot = array.get(end);
        int pIndex = start;
        int aux;
        for(int i = start; i < end; i++) {
            if(array.get(i) < pivot) {
                aux = array.get(i);
                array.set(i, array.get(pIndex));
                array.set(pIndex, aux);
                pIndex++;
            }
        }
        array.set(end, array.get(pIndex));
        array.set(pIndex, pivot);
        return pIndex;
    }

    private void quick(List<Integer> array, int start, int end) {
        if(start < end) {
            int pIndex = partition(array, start, end);
            quickParallel(array, start, pIndex - 1);
            quickParallel(array, pIndex + 1, end);
        }
    }

    private void quickParallel(List<Integer> array, int start, int end) {
        if(start < end) {
            int pIndex = partition(array, start, end);
            List<CompletableFuture<Void>> cf = new ArrayList<>();
            cf.add(CompletableFuture.supplyAsync(() -> {
                quickParallel(array, start, pIndex - 1);
                return null;
            }));
            cf.add(CompletableFuture.supplyAsync(() -> {
                quickParallel(array, pIndex + 1, end);
                return null;
            }));

            for(CompletableFuture<Void> e: cf){
                try {
                    e.get();
                } catch (InterruptedException | ExecutionException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
        }
    }
}
